# Use the official Python image from Docker Hub
FROM python:3.10

# Set the working directory inside the container
WORKDIR /drugapp

# Copy the requirements.txt file into the container
COPY requirements.txt .

# Install the Python dependencies
RUN pip install -r requirements.txt

# Copy the rest of your application files into the container
COPY . .

# Expose the port your app runs on (change it if necessary)
EXPOSE 5000

CMD ["/bin/bash"]