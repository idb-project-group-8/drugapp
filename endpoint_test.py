import unittest
from flask import Flask
from drugapp import app

class TestMyFlaskApp(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def all_drugs_endpoint(self):
        response = self.app.get('/get_drugs')
        self.assertEqual(response.status_code, 200)

    def specific_drug_endpoint(self):
        response = self.app.get('/get_drugs/Opioids')
        self.assertEqual(response.status_code, 200)

    def all_conditions_endpoint(self):
        response = self.app.get('/get_conditions')
        self.assertEqual(response.status_code, 200)

    def specific_condition_endpoint(self):
        response = self.app.get('/get_conditions/Arthritis')
        self.assertEqual(response.status_code, 200)

    def all_effects_endpoint(self):
        response = self.app.get('/get_effects')
        self.assertEqual(response.status_code, 200)

    def specific_effects_endpoint(self):
        response = self.app.get('/get_effects/Drowsiness')
        self.assertEqual(response.status_code, 200)
        

if __name__ == '__main__':
    unittest.main()