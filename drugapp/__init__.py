from flask import Flask, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from os import path
import requests

db = SQLAlchemy()
DB_NAME = "database.db"

app = Flask(__name__, static_folder='static', static_url_path='/static')
CORS(app)
app.config['SECRET_KEY'] = 'iknowiusedtobewildthatscauseiusedtobeyoung'
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{DB_NAME}'
db.init_app(app)

with app.app_context():
    db.create_all()

from .models import Drug, Condition, SideEffect

@app.route('/')
def home():
    return render_template('home.html')

@app.route('/drugs')
def drugs():
    return render_template('drugs.html')

@app.route('/conditions')
def conditions():
    return render_template('conditions.html')

@app.route('/side_effects')
def side_effects():
    return render_template('side_effects.html')

@app.route('/get_drugs', methods=['GET'])
def get_drugs():
    instances = Drug.query.all()
    # Serialize the instances to JSON
    instance_list = [{"id": instance.id, "name": instance.name, "route": instance.route, "manufacturer": instance.manufacturer, "conditions": instance.conditions, "sideEffects": instance.sideEffects, "img1": instance.img1, "img2": instance.img2} for instance in instances]

    # Return the JSON response
    return instance_list

@app.route('/get_drugs/<specific_drug>', methods=['GET'])
def specific_drug(specific_drug):
    instance = Drug.query.filter_by(name=specific_drug).first()
    if instance:
        return {"id": instance.id, "name": instance.name, "method": instance.method, "ingredients": instance.ingredients, "FDA": instance.FDA, "conditions": instance.conditions, "sideEffects": instance.sideEffects, "img1": instance.img1, "img2": instance.img2}
    return {"Error": "Drug not found"}


@app.route('/get_conditions', methods=['GET'])
def get_conditions():
    instances = Condition.query.all()
    # Serialize the instances to JSON
    instance_list = [{"id": instance.id, "name": instance.name, "severity": instance.severity, "location": instance.location, "drugs": instance.drugs, "sideEffects": instance.sideEffects, "img1": instance.img1, "img2": instance.img2} for instance in instances]

    # Return the JSON response
    return instance_list

@app.route('/get_conditions/<specific_condition>', methods=['GET'])
def specific_condition(specific_condition):
    instance = Condition.query.filter_by(name=specific_condition).first()
    if instance:
        return {"id": instance.id, "name": instance.name, "severity": instance.severity, "location": instance.location, "drugs": instance.drugs, "sideEffects": instance.sideEffects, "img1": instance.img1, "img2": instance.img2}
    return {"Error":"Condition not found"}

@app.route('/get_effects', methods=['GET'])
def get_effects():
    instances = SideEffect.query.all()
    # Serialize the instances to JSON
    instance_list = [{"id": instance.id, "name": instance.name, "severity": instance.severity, "bodyPart": instance.bodyPart, "conditions": instance.conditions, "drugs": instance.drugs, "img1": instance.img1, "img2": instance.img2} for instance in instances]

    # Return the JSON response
    return instance_list

@app.route('/get_effects/<specific_effect>', methods=['GET'])
def specific_effect(specific_effect):
    instance = SideEffect.query.filter_by(name=specific_effect).first()
    if instance:
        return {"id": instance.id, "name": instance.name, "severity": instance.severity, "bodyPart": instance.bodyPart, "conditions": instance.conditions, "drugs": instance.drugs, "img1": instance.img1, "img2": instance.img2}
    return {"Error":"Side Effect not found"}

@app.route('/populate')
def populate():
    popul = []


    names = set()
    for pop in popul:
        if pop['name'] not in names:
            names.add(pop['name'])

            url = "https://bing-image-search1.p.rapidapi.com/images/search"

            querystring = {"q":pop['name'],"count":"2"}

            headers = {
                "X-RapidAPI-Key": "aadd64d720msh94012fb00d0c4ccp170989jsnf62aa6235c8d",
                "X-RapidAPI-Host": "bing-image-search1.p.rapidapi.com"
            }

            response = requests.get(url, headers=headers, params=querystring)
            data = response.json()

            imgOne = data['value'][0]['thumbnailUrl']
            imgTwo = data['value'][1]['thumbnailUrl']

            sido = SideEffect(name=pop['name'], severity=pop['severity'], bodyPart=pop['bodyPart'], conditions=pop['conditions'], drugs=pop['drugs'], img1 = imgOne, img2 = imgTwo)
            db.session.add(sido)
            db.session.commit()


@app.route('/about')
def about():
    token = 'glpat-JLzx_b7dZ1ZKtFTK7NKZ'

    # Get commits data
    commits_url = "https://gitlab.com/api/v4/projects/50668186/repository/commits"
    params = {"per_page": 100}
    commits_response = requests.get(commits_url, headers={'PRIVATE-TOKEN': token}, params=params)
    commits_data = commits_response.json()

    # Create a dictionary to count commits by author
    commit_counts = {}
    commit_counts['total'] = 0
    commit_counts['sam'] = 0

    # Process commits data and calculate counts by author
    for commit in commits_data:
        author_name = commit['author_name']
        commit_counts[author_name] = commit_counts.get(author_name, 0) + 1
        commit_counts['total'] += 1

    print(commit_counts)

    issues_url = "https://gitlab.com/api/v4/projects/50668186/issues?state=closed"
    issues_response = requests.get(issues_url, headers={'PRIVATE-TOKEN': token}, params=params)
    issues_data = issues_response.json()

    # Create a dictionary to count closed issues by person
    closed_issue_counts = {}
    closed_issue_counts['total'] = 0
    closed_issue_counts['sam'] = 0

    # Process issues data and calculate counts by closed_by author
    for issue in issues_data:
        closed_by = issue['closed_by']
        if closed_by:
            closed_by_name = closed_by['name']
            closed_issue_counts[closed_by_name] = closed_issue_counts.get(closed_by_name, 0) + 1
            closed_issue_counts['total'] += 1

    return render_template('about.html', commits=commit_counts, issues=closed_issue_counts)


if not path.exists('drugapp/' + DB_NAME):
    app.app_context().push()
    db.create_all()

if __name__ == "__main__":
    app.run(port=5000, debug=True)
