from . import db
from sqlalchemy.sql import func

class Drug(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True)
    route = db.Column(db.String(150))
    manufacturer = db.Column(db.String(500))
    conditions = db.Column(db.String(1000))
    sideEffects = db.Column(db.String(1000))
    img1 = db.Column(db.String(1000))
    img2 = db.Column(db.String(1000))
    def __repr__(self):
        return f'< id: {self.id}, Drug: {self.name} >'
    
class Condition(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True)
    severity = db.Column(db.String(500))
    location = db.Column(db.String(500))
    drugs = db.Column(db.String(1000))
    sideEffects = db.Column(db.String(1000))
    img1 = db.Column(db.String(1000))
    img2 = db.Column(db.String(1000))
    def __repr__(self):
        return f'< id: {self.id}, Condition: {self.name} >'

class SideEffect(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True)
    severity = db.Column(db.String(500))
    bodyPart = db.Column(db.String(500))
    conditions = db.Column(db.String(1000))
    drugs = db.Column(db.String(500))
    img1 = db.Column(db.String(1000))
    img2 = db.Column(db.String(1000))
    def __repr__(self):
        return f'< id: {self.id}, Side Effect: {self.name} >'