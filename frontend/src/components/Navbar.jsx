import { useState } from "react";
import "../styles/Navbar.css";

const navbarButtons = [
  "Home",
  "About",
  "Conditions/Addictions",
  "Drugs/Medications",
  "Side-Effects",
  "Visualizations",
  "Provider Visuals",
];

const links = [
  "",
  "About",
  "Conditions",
  "Drugs",
  "SideEffects",
  "Visualizations",
  "Provider_Visualizations",
];

function Navbar() {
  const [search, setSearch] = useState("");

  const handleChange = (event) => {
    setSearch(event.target.value);
  };

  const handleClick = (event) => {
    event.preventDefault();
    setSearch("");
    window.location.href = "/search/" + search;
  };

  return (
    <>
      <nav className="container-fluid my_navbar">
        <div className="row">
          {navbarButtons.map((button, index) => (
            <div
              key={button + "button"}
              className={index == 0 ? "col-auto offset-1" : "col-auto"}
            >
              <a role="button" href={"/" + links[index]}>
                <div className="navbar_link">{button}</div>
              </a>
            </div>
          ))}
          ;
          <form className="col-auto offset-1">
            <div className="input-group">
              <input
                type="search"
                className="form-control"
                placeholder="Search"
                onChange={handleChange}
              ></input>
              <button className="btn btn-primary" onClick={handleClick}>
                Submit
              </button>
            </div>
          </form>
        </div>
      </nav>
    </>
  );
}

export default Navbar;
