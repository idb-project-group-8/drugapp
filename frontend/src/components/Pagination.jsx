function Pagination({ numPages, page, setPage }) {
  let pageNumbers = [];
  for (let i = 1; i <= numPages; ++i) {
    pageNumbers.push(i);
  }

  const updatePage = (newPage) => {
    if (newPage < 1) {
      newPage = 1;
    }
    if (newPage > numPages) {
      newPage = numPages;
    }
    if (newPage !== page) {
      setPage(newPage);
    }
  };

  return (
    <div className="pagination">
      <button className="page-link" onClick={() => updatePage(1)}>
        &laquo;
      </button>
      <button className="page-link" onClick={() => updatePage(page - 1)}>
        &lsaquo;
      </button>
      {pageNumbers.map((num) => {
        return (
          <button
            key={"button " + num}
            className={"page-link" + (num === page ? " active" : "")}
            onClick={() => updatePage(num)}
          >
            {num}
          </button>
        );
      })}
      <button className="page-link" onClick={() => updatePage(page + 1)}>
        &rsaquo;
      </button>
      <button className="page-link" onClick={() => updatePage(numPages)}>
        &raquo;
      </button>
    </div>
  );
}

export default Pagination;
