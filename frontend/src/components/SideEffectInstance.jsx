import { useParams } from "react-router-dom";
import { getConditions, getDrugs, getSideEffect } from "../Database";
import "../styles/Instance.css";
import { useState, useEffect } from "react";
import {} from "react";

function SideEffectInstance() {
  const { se_id } = useParams();

  const [sideEffect, setSideEffect] = useState(null);
  const [drugs, setDrugs] = useState([]);
  const [conditions, setConditions] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const se = await getSideEffect(se_id);
      const d = await getDrugs();
      const c = await getConditions();
      setSideEffect(se);
      setDrugs(d);
      setConditions(c);
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  return (
    <div className="instance_page">
      <h1>{sideEffect.name}</h1>
      <img
        src={sideEffect.img1}
        className="instance-page-img"
        alt={"Image of " + sideEffect.name}
      />
      <img
        src={sideEffect.img2}
        className="instance-page-img"
        alt={"Image of " + sideEffect.name}
      />
      <p>Severity: {sideEffect.severity}</p>
      <p>Body Part: {sideEffect.bodyPart}</p>
      <p>
        Drugs:{" "}
        {sideEffect.drugs.length === 0
          ? "None"
          : sideEffect.drugs.split(", ").map((id) => {
              const drug = drugs.find((d) => d.name === id);
              return (
                <a key={drug.name} role="button" href={"/Drug/" + drug.name}>
                  {drug.name},{" "}
                </a>
              );
            })}
      </p>
      <p>
        Conditions:{" "}
        {sideEffect.conditions.length === 0
          ? "None"
          : sideEffect.conditions.split(", ").map((id) => {
              const condition = conditions.find((c) => c.name === id);
              return (
                <a
                  key={condition.name}
                  role="button"
                  href={"/Condition/" + condition.name}
                >
                  {condition.name},{" "}
                </a>
              );
            })}
      </p>
    </div>
  );
}

export default SideEffectInstance;
