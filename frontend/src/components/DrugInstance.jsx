import { useParams } from "react-router-dom";
import { getConditions, getDrug, getSideEffects } from "../Database";
import "../styles/Instance.css";
import { useEffect, useState } from "react";

function DrugInstance() {
  const { drug_id } = useParams();

  const [drug, setDrug] = useState(null);
  const [conditions, setConditions] = useState([]);
  const [sideEffects, setSideEffects] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const d = await getDrug(drug_id);
      const c = await getConditions();
      const se = await getSideEffects();
      setDrug(d);
      setConditions(c);
      setSideEffects(se);
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  return (
    <div className="instance_page">
      <h1>{drug.name}</h1>
      <img
        src={drug.img1}
        className="instance-page-img"
        alt={"Image of " + drug.name}
      />
      <img
        src={drug.img2}
        className="instance-page-img"
        alt={"Image of " + drug.name}
      />
      <p>Route: {drug.route}</p>
      <p>Manufacturer: {drug.manufacturer}</p>
      <p>
        Conditions:{" "}
        {drug.conditions.length === 0
          ? "None"
          : drug.conditions.split(", ").map((id) => {
              const condition = conditions.find((c) => c.name === id);
              return (
                <a
                  key={condition.name}
                  role="button"
                  href={"/Condition/" + condition.name}
                >
                  {condition.name},{" "}
                </a>
              );
            })}
      </p>
      <p>
        SideEffects:{" "}
        {drug.sideEffects.length === 0
          ? "None"
          : drug.sideEffects.split(", ").map((id) => {
              const sideEffect = sideEffects.find((se) => se.name === id);
              return (
                <a
                  key={sideEffect.name}
                  role="button"
                  href={"/SideEffect/" + sideEffect.name}
                >
                  {sideEffect.name},{" "}
                </a>
              );
            })}
      </p>
    </div>
  );
}

export default DrugInstance;
