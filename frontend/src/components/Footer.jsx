import "../styles/Footer.css";

function Footer() {
  return (
    <footer>
      <div className="footer_text">
        DrugProfiles https://drugprofiles.me © Copyright 2023
      </div>
    </footer>
  );
}

export default Footer;
