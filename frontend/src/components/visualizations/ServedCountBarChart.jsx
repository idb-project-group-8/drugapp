import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

function ServedCountBarChart({ data }) {
  const [providers, neighborhoods, restaurants] = data;
  let servedCountData = [];
  for (let i = 0; i < providers.length; ++i) {
    let index = servedCountData.length;
    for (let j = 0; j < servedCountData.length; ++j) {
      if (providers[i].population_served_count === servedCountData[j].name) {
        index = j;
        break;
      }
    }
    if (index == servedCountData.length) {
      servedCountData.push({
        name: providers[i].population_served_count,
        value: 0,
      });
    }
    ++servedCountData[index].value;
  }
  servedCountData.sort((a, b) => a.name - b.name);
  return (
    <BarChart width={600} height={400} data={servedCountData}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="value" fill="#8884d8" />
    </BarChart>
  );
}

export default ServedCountBarChart;
