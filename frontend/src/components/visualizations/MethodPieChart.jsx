import { PieChart, Pie, Tooltip, Legend, Cell } from "recharts";

function MethodPieChart({ data }) {
  const [conditions, drugs, sideEffects] = data;
  let methodData = [];
  for (let i = 0; i < drugs.length; ++i) {
    let method = drugs[i].route.toLowerCase();
    let index = methodData.length;
    for (let j = 0; j < methodData.length; ++j) {
      if (method === methodData[j].name) {
        index = j;
        break;
      }
    }
    if (index === methodData.length) {
      methodData.push({ name: method, value: 0 });
    }
    ++methodData[index].value;
  }
  const colors = ["#007313", "#FF9422", "#D92600", "#000000"];
  return (
    <PieChart width={400} height={400}>
      <Pie
        data={methodData}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        outerRadius={150}
      >
        {methodData.map((entry, index) => (
          <Cell key={entry.name} fill={colors[index]} />
        ))}
      </Pie>
      <Tooltip />
      <Legend />
    </PieChart>
  );
}

export default MethodPieChart;
