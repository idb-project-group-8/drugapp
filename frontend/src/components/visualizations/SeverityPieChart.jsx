import { PieChart, Pie, Tooltip, Legend, Cell } from "recharts";

function SeverityPieChart({ data }) {
  const [conditions, drugs, sideEffects] = data;
  let severityData = [
    { name: "Mild", value: 0 },
    { name: "Moderate", value: 0 },
    { name: "Severe", value: 0 },
  ];
  for (let i = 0; i < conditions.length; ++i) {
    let sevLower = conditions[i].severity.toLowerCase();
    if (sevLower.includes("mild")) ++severityData[0].value;
    if (sevLower.includes("moderate")) ++severityData[1].value;
    if (sevLower.includes("severe")) ++severityData[2].value;
  }
  for (let i = 0; i < sideEffects.length; ++i) {
    let sevLower = sideEffects[i].severity.toLowerCase();
    let containsMild = sevLower.includes("mild");
    let containsModerate = sevLower.includes("moderate");
    let containsSevere = sevLower.includes("severe");
    if (containsMild) ++severityData[0].value;
    if (containsSevere) ++severityData[2].value;
    if (containsModerate || (containsMild && containsSevere))
      ++severityData[1].value;
  }
  const colors = ["#007313", "#FF9422", "#D92600"];
  return (
    <PieChart width={400} height={400}>
      <Pie
        data={severityData}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        outerRadius={150}
        label
      >
        {severityData.map((entry, index) => (
          <Cell key={entry.name} fill={colors[index]} />
        ))}
      </Pie>
      <Tooltip />
      <Legend />
    </PieChart>
  );
}

export default SeverityPieChart;
