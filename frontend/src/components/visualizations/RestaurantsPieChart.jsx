import { PieChart, Pie, Tooltip, Legend, Cell } from "recharts";

function RestaurantsPieChart({ data }) {
  const [providers, neighborhoods, restaurants] = data;
  let restaurantData = [];
  for (let i = 0; i < restaurants.length; ++i) {
    if (restaurants[i].price_level === "") continue;
    let index = restaurantData.length;
    for (let j = 0; j < restaurantData.length; ++j) {
      if (restaurants[i].price_level === restaurantData[j].name) {
        index = j;
        break;
      }
    }
    if (index === restaurantData.length) {
      restaurantData.push({ name: restaurants[i].price_level, value: 0 });
    }
    ++restaurantData[index].value;
  }
  const colors = ["#007313", "#FF9422", "#D92600", "#000000"];
  return (
    <PieChart width={400} height={400}>
      <Pie
        data={restaurantData}
        dataKey="value"
        nameKey="name"
        cx="50%"
        cy="50%"
        outerRadius={150}
      >
        {restaurantData.map((entry, index) => (
          <Cell key={entry.name} fill={colors[index]} />
        ))}
      </Pie>
      <Tooltip />
      <Legend />
    </PieChart>
  );
}

export default RestaurantsPieChart;
