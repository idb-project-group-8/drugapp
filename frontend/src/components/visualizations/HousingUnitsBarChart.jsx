import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

function HousingUnitsBarChart({ data }) {
  const [providers, neighborhoods, restaurants] = data;
  let housingData = [];
  console.log("neighborhoods.length: ", neighborhoods.length);
  for (let i = 0; i < neighborhoods.length; ++i) {
    housingData[i] = { name: i, value: neighborhoods[i].housing_Units };
  }
  return (
    <BarChart width={600} height={400} data={housingData}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="value" fill="#8884d8" />
    </BarChart>
  );
}

export default HousingUnitsBarChart;
