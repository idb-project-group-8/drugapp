import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

function LocationBarChart({ data }) {
  const [conditions, drugs, sideEffects] = data;
  let locationData = [];
  for (let i = 0; i < conditions.length; ++i) {
    let index = locationData.length;
    for (let j = 0; j < locationData.length; ++j) {
      if (conditions[i].location === locationData[j].name) {
        index = j;
        break;
      }
    }
    if (index === locationData.length) {
      locationData.push({ name: conditions[i].location, value: 0 });
    }
    ++locationData[index].value;
  }
  for (let i = 0; i < sideEffects.length; ++i) {
    let index = locationData.length;
    for (let j = 0; j < locationData.length; ++j) {
      if (sideEffects[i].bodyPart === locationData[j].name) {
        index = j;
        break;
      }
    }
    if (index === locationData.length) {
      locationData.push({ name: sideEffects[i].bodyPart, value: 0 });
    }
    ++locationData[index].value;
  }
  return (
    <BarChart width={600} height={400} data={locationData}>
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
      <Legend />
      <Bar dataKey="value" fill="#8884d8" />
    </BarChart>
  );
}

export default LocationBarChart;
