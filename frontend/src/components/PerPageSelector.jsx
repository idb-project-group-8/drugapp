function PerPageSelector({ setPerPage, setPage }) {
  return (
    <div className="container w-25">
      <label>Results per Page:</label>
      <select className="form-select">
        <option
          onClick={() => {
            setPerPage(10);
            setPage(1);
          }}
        >
          10
        </option>
        <option
          onClick={() => {
            setPerPage(20);
            setPage(1);
          }}
        >
          20
        </option>
        <option
          onClick={() => {
            setPerPage(50);
            setPage(1);
          }}
        >
          50
        </option>
      </select>
    </div>
  );
}

export default PerPageSelector;
