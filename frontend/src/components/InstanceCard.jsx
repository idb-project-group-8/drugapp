function InstanceCard({ instance, type }) {
  const i = instance;
  console.log("i.highlighted: ", i.highlighted);
  return (
    <div className="col">
      <div className="instance">
        <div className="card-img-cnt">
          <img
            src={i.img1}
            className="instance-img"
            alt={"Image of " + i.name}
          />
        </div>
        <div className="card-name-header">{getHighlightedName(i)}</div>
        {type === "Drug" ? (
          <>
            <div className="card-info">{getHighlightedRoute(i)}</div>
            <div className="card-info">{getHighlightedManufacturer(i)}</div>
            <div className="card-info">{getHighlightedConditions(i)}</div>
            <div className="card-info">{getHighlightedSideEffects(i)}</div>
          </>
        ) : type === "Condition" ? (
          <>
            <div className="card-info">{getHighlightedSeverity(i)}</div>
            <div className="card-info">{getHighlightedLocation(i)}</div>
            <div className="card-info">{getHighlightedDrugs(i)}</div>
            <div className="card-info">{getHighlightedSideEffects(i)}</div>
          </>
        ) : (
          <>
            <div className="card-info">{getHighlightedSeverity(i)}</div>
            <div className="card-info">{getHighlightedBodyPart(i)}</div>
            <div className="card-info">{getHighlightedConditions(i)}</div>
            <div className="card-info">{getHighlightedDrugs(i)}</div>
          </>
        )}
        <a
          className="btn btn-primary instance-btn"
          href={"/" + type + "/" + i.name}
        >
          Learn More
        </a>
      </div>
    </div>
  );
}

export default InstanceCard;

function getHighlightedName(i) {
  if (i.highlighted === undefined) return <h2>{i.name}</h2>;
  const name = i.name.split(new RegExp(`(${i.highlighted.join("|")})`, "gi"));
  return (
    <h2>
      {name.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </h2>
  );
}

function getHighlightedSeverity(i) {
  if (i.highlighted === undefined) return <p>Severity: {i.severity}</p>;
  const severity = i.severity.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  return (
    <p>
      Severity:{" "}
      {severity.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedRoute(i) {
  if (i.highlighted === undefined) return <p>Route: {i.route}</p>;
  const route = i.route.split(new RegExp(`(${i.highlighted.join("|")})`, "gi"));
  return (
    <p>
      Route:{" "}
      {route.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedManufacturer(i) {
  if (i.highlighted === undefined) return <p>Manufacturer: {i.manufacturer}</p>;
  const manufacturer = i.manufacturer.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  return (
    <p>
      Manufacturer:{" "}
      {manufacturer.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedBodyPart(i) {
  if (i.highlighted === undefined) return <p>Body Part: {i.bodyPart}</p>;
  const bodyPart = i.bodyPart.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  return (
    <p>
      Body Part:{" "}
      {bodyPart.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedLocation(i) {
  if (i.highlighted === undefined) return <p>Location: {i.location}</p>;
  const location = i.location.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  return (
    <p>
      Location:{" "}
      {location.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedConditions(i) {
  if (i.highlighted === undefined) return <p>Conditions: {i.conditions}</p>;
  const conditions = i.conditions.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  return (
    <p>
      Conditions:{" "}
      {conditions.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedDrugs(i) {
  if (i.highlighted === undefined) return <p>Drugs: {i.drugs}</p>;
  const drugs = i.drugs.split(new RegExp(`(${i.highlighted.join("|")})`, "gi"));
  return (
    <p>
      Drugs:{" "}
      {drugs.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}

function getHighlightedSideEffects(i) {
  if (i.highlighted === undefined) return <p>Side Effects: {i.sideEffects}</p>;
  const sideEffects = i.sideEffects.split(
    new RegExp(`(${i.highlighted.join("|")})`, "gi")
  );
  console.log("sideEffects: ", sideEffects);
  return (
    <p>
      Side Effects:{" "}
      {sideEffects.map((x, index) =>
        i.highlighted.includes(x.toLowerCase()) ? (
          <span key={index} style={{ backgroundColor: "yellow" }}>
            {x}
          </span>
        ) : (
          <span key={index}>{x}</span>
        )
      )}
    </p>
  );
}
