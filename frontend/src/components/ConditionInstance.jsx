import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { getCondition, getDrugs, getSideEffects } from "../Database";
import "../styles/Instance.css";

function ConditionInstance() {
  const { cond_id } = useParams();

  const [cond, setCond] = useState(null);
  const [drugs, setDrugs] = useState([]);
  const [sideEffects, setSideEffects] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const c = await getCondition(cond_id);
      const d = await getDrugs();
      const se = await getSideEffects();
      setCond(c);
      setDrugs(d);
      setSideEffects(se);
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  return (
    <div className="instance_page">
      <h1>{cond.name}</h1>
      <img
        src={cond.img1}
        className="instance-page-img"
        alt={"Image of " + cond.name}
      />
      <img
        src={cond.img2}
        className="instance-page-img"
        alt={"Image of " + cond.name}
      />
      <p>Severity: {cond.severity}</p>
      <p>Location: {cond.location}</p>
      <p>
        Drugs:{" "}
        {cond.drugs.length === 0
          ? "None"
          : cond.drugs.split(", ").map((id) => {
              const drug = drugs.find((d) => d.name === id);
              return (
                <a key={drug.name} role="button" href={"/Drug/" + drug.name}>
                  {drug.name},{" "}
                </a>
              );
            })}
      </p>
      <p>
        Side Effects:{" "}
        {cond.sideEffects.length === 0
          ? "None"
          : cond.sideEffects.split(", ").map((id) => {
              const se = sideEffects.find((se) => se.name === id);
              return (
                <a key={se.name} role="button" href={"/SideEffect/" + se.name}>
                  {se.name},{" "}
                </a>
              );
            })}
      </p>
    </div>
  );
}

export default ConditionInstance;
