import axios from "axios";

import MattImg from "../images/matt_img.jpg";
import RayanImg from "../images/rayan_img.png";
import SamImg from "../images/sam_img.jpg";
import BrandonImg from "../images/brandon_img.jpg";

export const TeamMembers = [
  {
    name: "Matthew Larson",
    image: MattImg,
    role: "Backend",
    email: "melars@utexas.edu",
    gitlab: "melars74",
    about:
      "I am a currently a Junior, and I work as a code mentor for Breakout Mentors. I just got two guinea pigs so some of my free time is just spent hanging out with them, but I'm also involved in Longhorn Gaming with Overwatch.",
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
  {
    name: "Rayan Ali",
    image: RayanImg,
    role: "Backend",
    email: "xrayanali202@gmail.com",
    gitlab: "xrayanali202",
    about:
      "I'm currently a senior at UT and plan to work as a fulltime SWE when I graduate this Spring. I hope to make an amazing website that prevents drug abuse in small towns.",
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
  {
    name: "Sam Lu",
    image: SamImg,
    role: "Frontend",
    email: "samlu@utexas.edu",
    gitlab: "samlu1",
    about:
      "I am an undergraduate student at The University of Texas at Austin studying Computer Science and Mathematics. My interests are in machine learning, data science, full-stack development, and cybersecurity. In my free time, I enjoy table tennis 🏓, tennis 🎾, trying new restaurants 😋, reading 📖, and listening to music 🎵.",
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
  {
    name: "Brandon Boettcher",
    image: BrandonImg,
    role: "Frontend",
    email: "boettcherb@utexas.edu",
    gitlab: "boettcherb",
    about:
      "I am a senior CS major and I just finished an internship in IT security for Reef Technologies. In my free time I play the guitar, chess, and video games such as Dark Souls. I enjoy going to Longhorn football, basketball, and volleyball games.",
    commits: 0,
    issues: 0,
    unitTests: 0,
  },
];

export const getGitLabCommits = async () => {
  let commits = [0, 0, 0, 0];
  const response = await axios.get(
    "https://gitlab.com/api/v4/projects/50668186/repository/commits?page=1&per_page=1000"
  );
  const commitData = response.data;
  for (let i = 0; i < commitData.length; ++i) {
    for (let j = 0; j < TeamMembers.length; ++j) {
      if (
        commitData[i]["author_name"].toLowerCase() ===
          TeamMembers[j]["name"].toLowerCase() ||
        commitData[i]["author_email"] === TeamMembers[j]["email"]
      ) {
        commits[j] += 1;
        break;
      }
    }
  }
  return commits;
};

export const getGitLabIssues = async () => {
  let issues = [0, 0, 0, 0];
  const response = await axios.get(
    "https://gitlab.com/api/v4/projects/50668186/issues?scope=all&per_page=1000"
  );
  const issueData = response.data;
  // Try to find who closed the issue. If the member who closed the issue
  // cannot be determined, try to find who created the issue.
  for (let i = 0; i < issueData.length; ++i) {
    const username =
      issueData[i]["closed_by"] === null
        ? issueData[i]["author"]["username"]
        : issueData[i]["closed_by"]["username"];
    let foundUser = false;
    for (let j = 0; j < TeamMembers.length; ++j) {
      if (username === TeamMembers[j]["gitlab"]) {
        issues[j] += 1;
        foundUser = true;
        break;
      }
    }
    if (!foundUser) {
      for (let j = 0; j < TeamMembers.length; ++j) {
        if (issueData[i]["author"]["username"] === TeamMembers[j]["gitlab"]) {
          issues[j] += 1;
          break;
        }
      }
    }
  }
  return issues;
};

export function GroupMember(props) {
  return (
    <div className="col">
      <div className="member">
        <h4>{props.member.name}</h4>
        <img src={props.member.image}></img>
        <h6>Role: {props.member.role + " Developer"}</h6>
        <h6>Email: {props.member.email}</h6>
        <h6>GitLab ID: {props.member.gitlab}</h6>
        <p>{props.member.about}</p>
        <h6>Commits: {props.member.commits}</h6>
        <h6>Issues: {props.member.issues}</h6>
        <h6>Unit Tests: {props.member.unitTests}</h6>
      </div>
    </div>
  );
}
