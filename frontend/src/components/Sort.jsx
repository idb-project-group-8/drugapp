function Sort({ options, func }) {
  return (
    <div className="container w-25">
      <label>Sort By:</label>
      <select className="form-select">
        {options.map((option) => {
          return (
            <option
              onClick={() => {
                func(option);
              }}
              key={option}
            >
              {option}
            </option>
          );
        })}
      </select>
    </div>
  );
}

export default Sort;
