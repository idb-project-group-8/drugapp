import { useState, useEffect } from "react";
import { getProviders, getNeighborhoods, getRestaurants } from "../Database";
import ServedCountBarChart from "../components/visualizations/ServedCountBarChart";
import HousingUnitsBarChart from "../components/visualizations/HousingUnitsBarChart";
import RestaurantsPieChart from "../components/visualizations/RestaurantsPieChart";

function ProviderVisualizations() {
  const [providers, setProviders] = useState([]);
  const [neighborhoods, setNeighborhoods] = useState([]);
  const [restaurants, setRestaurants] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const p = await getProviders();
      const n = await getNeighborhoods();
      const r = await getRestaurants();
      setProviders(p);
      setNeighborhoods(n);
      setRestaurants(r);
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>Loading...</h1>;
  }

  const data = [providers, neighborhoods, restaurants];
  return (
    <div id="main_content">
      <h1>Provider Visualizations</h1>
      <h3 className="centered-h3">Population Served Per Water Provider</h3>
      <div className="centered">
        <ServedCountBarChart data={data} />
      </div>
      <h3 className="centered-h3">Housing Units Per Neighborhood</h3>
      <div className="centered">
        <HousingUnitsBarChart data={data} />
      </div>
      <h3 className="centered-h3">Restaruants by Price</h3>
      <div className="centered">
        <RestaurantsPieChart data={data} />
      </div>
    </div>
  );
}

export default ProviderVisualizations;
