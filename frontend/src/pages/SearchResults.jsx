import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { getConditions, getDrugs, getSideEffects } from "../Database";
import InstanceCard from "../components/InstanceCard";
import "../styles/SearchResults.css";

function SearchResults() {
  const { sid } = useParams();
  const terms = sid.split(" ");

  for (let i = 0; i < terms.length; ++i) {
    terms[i] = terms[i].toLowerCase();
  }

  const [conditions, setConditions] = useState([]);
  const [drugs, setDrugs] = useState([]);
  const [sideEffects, setSideEffects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [tab, setTab] = useState("Condition");

  useEffect(() => {
    const fetchData = async () => {
      const c = await getConditions();
      const d = await getDrugs();
      const se = await getSideEffects();
      setConditions(refineSearch(c, 1, terms));
      setDrugs(refineSearch(d, 2, terms));
      setSideEffects(refineSearch(se, 3, terms));
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>Loading...</h1>;
  }

  const instances =
    tab === "Condition" ? conditions : tab === "Drug" ? drugs : sideEffects;
  return (
    <>
      <div className="container tab-buttons">
        <div className="row">
          <div className="col d-flex justify-content-center">
            <button
              className={
                "btn btn-" + (tab === "Condition" ? "primary" : "dark")
              }
              onClick={() => setTab("Condition")}
            >
              Conditions/Addictions
            </button>
          </div>
          <div className="col d-flex justify-content-center">
            <button
              className={"btn btn-" + (tab === "Drug" ? "primary" : "dark")}
              onClick={() => setTab("Drug")}
            >
              Drugs/Medications
            </button>
          </div>
          <div className="col d-flex justify-content-center">
            <button
              className={
                "btn btn-" + (tab === "SideEffect" ? "primary" : "dark")
              }
              onClick={() => setTab("SideEffect")}
            >
              Side-Effects
            </button>
          </div>
        </div>
      </div>
      <h1>Results: {instances.length}</h1>
      <div className="container instanceCard_box">
        <div className="row row-cols-5 g-4">
          {instances.map((s) => {
            const new_s = { ...s, highlighted: terms };
            return (
              <InstanceCard key={new_s.name} instance={new_s} type={tab} />
            );
          })}
        </div>
      </div>
    </>
  );
}

export default SearchResults;

function refineSearch(instances, type, terms) {
  let refined = [];
  instances.forEach((instance) => {
    terms.forEach((term) => {
      let found =
        refined.length > 0 && refined[refined.length - 1].id === instance.id;
      // name
      if (!found && instance.name.toLowerCase().includes(term)) {
        found = true;
        refined.push(instance);
        return;
      }
      // severity
      if (
        !found &&
        (type === 1 || type === 3) &&
        instance.severity.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // location
      if (
        !found &&
        type === 1 &&
        instance.location.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // bodyPart
      if (
        !found &&
        type === 3 &&
        instance.bodyPart.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // route
      if (!found && type === 2 && instance.route.toLowerCase().includes(term)) {
        found = true;
        refined.push(instance);
        return;
      }
      // manufacturer
      if (
        !found &&
        type === 2 &&
        instance.manufacturer.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // conditions
      if (
        !found &&
        (type === 2 || type === 3) &&
        instance.conditions.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // drugs
      if (
        !found &&
        (type === 1 || type === 3) &&
        instance.drugs.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
      // sideEffects
      if (
        !found &&
        (type === 1 || type === 2) &&
        instance.sideEffects.toLowerCase().includes(term)
      ) {
        found = true;
        refined.push(instance);
        return;
      }
    });
  });
  return refined;
}
