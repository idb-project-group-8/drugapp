import "../styles/Home.css";

function Home() {
  return (
    <div id="main_content" className="custom-background">
      <div className="home-page-title">
        <h1>Drug Profiles</h1>
        <p>
          Learn about the Drugs, Conditions, and Side Effects affecting your
          community
        </p>
      </div>
    </div>
  );
}

export default Home;
