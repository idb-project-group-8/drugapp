import { useEffect, useState } from "react";
import { getDrugs } from "../Database";
import InstanceCard from "../components/InstanceCard";
import Pagination from "../components/Pagination";
import PerPageSelector from "../components/PerPageSelector";
import "../styles/Model.css";
import Filter from "../components/Filter";
import Sort from "../components/Sort";

const filterOptions = [
  "None",
  "Route: Oral",
  "Route: Injection",
  "Route: Inhalation",
  "Route: Topical",
];

const sortOptions = [
  "None",
  "Name (ascending)",
  "Name (descending)",
  "Route (ascending)",
  "Route (descending)",
  "Manufacturer (ascending)",
  "Manufacturer (descending)",
];

function Drugs() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [allDrugs, setAllDrugs] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filterBy, setFilterBy] = useState("None");
  const [sortBy, setSortBy] = useState("None");

  useEffect(() => {
    const fetchDrugs = async () => {
      const fetchedDrugs = await getDrugs();
      setAllDrugs(fetchedDrugs);
      setLoading(false);
    };
    setLoading(true);
    fetchDrugs();
  }, []);

  const handleFilter = (option) => {
    setFilterBy(option);
    setPage(1);
  };

  const handleSort = (option) => {
    setSortBy(option);
    setPage(1);
  };

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  let drugs = allDrugs;
  drugs = drugs.filter((x) => {
    if (filterBy.startsWith("Route")) {
      return x.route === filterBy.substring(7);
    } else {
      return true;
    }
  });
  if (sortBy !== "None") {
    drugs.sort((a, b) => {
      if (sortBy === "Name (ascending)") return a.name.localeCompare(b.name);
      if (sortBy === "Name (descending)") return -a.name.localeCompare(b.name);
      if (sortBy === "Route (ascending)") return a.route.localeCompare(b.route);
      if (sortBy === "Route (descending)")
        return -a.route.localeCompare(b.route);
      if (sortBy === "Manufacturer (ascending)")
        return a.manufacturer.localeCompare(b.manufacturer);
      if (sortBy === "Manufacturer (descending)")
        return -a.manufacturer.localeCompare(b.manufacturer);
      throw "Error: Unknown Sort";
    });
  }

  const numPages = Math.ceil(drugs.length / perPage);
  return (
    <div id="instance_card_container">
      <h1>Drugs / Medications</h1>
      <h5 className="total">
        {filterBy === "None" ? "Total number of Drugs: " : "Results: "}
        {drugs.length}
      </h5>
      <PerPageSelector setPerPage={setPerPage} setPage={setPage} />
      <Filter options={filterOptions} func={handleFilter} />
      <Sort options={sortOptions} func={handleSort} />
      <Pagination numPages={numPages} page={page} setPage={setPage} />
      <div className="container">
        <div className="row row-cols-5 g-4">
          {drugs.map((d, index) => {
            if (index >= (page - 1) * perPage && index < page * perPage) {
              return <InstanceCard key={d.name} instance={d} type="Drug" />;
            }
          })}
        </div>
      </div>
      <Pagination numPages={numPages} page={page} setPage={setPage} />
    </div>
  );
}

export default Drugs;
