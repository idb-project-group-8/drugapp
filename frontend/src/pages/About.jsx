import { useState, useEffect } from "react";
import {
  GroupMember,
  getGitLabCommits,
  getGitLabIssues,
} from "../components/GroupMember";
import "../styles/About.css";
import { TeamMembers } from "../components/GroupMember";

import ReactImg from "../images/tools/react_logo.png";
import FlaskImg from "../images/tools/flask_logo.png";
import PostmanImg from "../images/tools/postman_logo.png";
import BootstrapImg from "../images/tools/bootstrap_logo.jpg";
import AWSImg from "../images/tools/aws_logo.png";
import NamecheapImg from "../images/tools/namecheap_logo.png";
import ZoomImg from "../images/tools/zoom_logo.png";

function About() {
  let [matt, rayan, sam, brandon] = TeamMembers;
  const [commits, setCommits] = useState([]);
  const [issues, setIssues] = useState([]);
  let unitTests = [0, 6, 0, 11];
  rayan.unitTests = 6;
  brandon.unitTests = 11;

  // Get Commit Data from GitLab api every time the about page is reloaded
  useEffect(() => {
    const getCommits = async () => {
      const commitData = await getGitLabCommits();
      setCommits(commitData);
      TeamMembers.map((member, index) => {
        member.commits = commitData[index];
      });
    };
    getCommits();
  }, []);

  // Get Issue Data from GitLab api every time the about page is reloaded
  useEffect(() => {
    const getIssues = async () => {
      const issueData = await getGitLabIssues();
      setIssues(issueData);
      TeamMembers.map((member, index) => {
        member.issues = issueData[index];
      });
    };
    getIssues();
  }, []);

  return (
    <div id="main_content">
      <div id="main_about_content">
        <h1>About Drug Profiles</h1>
        <h2>What is Drug Profiles?</h2>
        <p>
          A website providing information on conditions, treatments, and
          side-effects specifically geared-towards the Appalachian communities
          which struggle with drug addiction that has been neglected since the
          20th century.
        </p>
        <h2>Who does Drug Profiles benefit?</h2>
        <p>
          We aim to serve communities in the Appalchian reagion that are
          predominantly White with blue-collared jobs, are an aging population,
          and have struggled economically due to decline of traditional
          industries like coal and manufacturing.
        </p>
        <h2>What is Drug Profiles purpose?</h2>
        <p>
          This information could potentially help those in affected regions
          understand their conditions and treatment possibilities and side
          effects better to avoid misuse of drugs/medicine.
        </p>
        <h2>Interesting Data Conclusions</h2>
        <p>
          After getting all of our data together, we found that it was pretty
          cool how these seemingly disparate instances could directly link to
          each other. For example, we could take something like Heroin and link
          that to Headaches, which then links to Desoxyn and then ADHD.
        </p>
        <h1>Group Members:</h1>
        <div className="container">
          <div className="row row-cols-2 g-4">
            <GroupMember member={rayan} />
            <GroupMember member={brandon} />
          </div>
        </div>
        <h2>Overall Stats:</h2>
        <div className="container">
          <div className="row">
            <div className="col-2">
              <p>Commits: {commits.reduce((a, b) => a + b, 0)}</p>
            </div>
            <div className="col-2">
              <p>Issues: {issues.reduce((a, b) => a + b, 0)}</p>
            </div>
            <div className="col-2">
              <p>Unit Tests: {unitTests.reduce((a, b) => a + b, 0)}</p>
            </div>
          </div>
        </div>
        <h2>Tools Used:</h2>
        <div className="container">
          <div className="row row-cols-5 g-4">
            <div className="col">
              <img src={ReactImg}></img>
            </div>
            <div className="col">
              <img src={FlaskImg}></img>
            </div>
            <div className="col">
              <img src={PostmanImg}></img>
            </div>
            <div className="col">
              <img src={BootstrapImg}></img>
            </div>
            <div className="col">
              <img src={AWSImg}></img>
            </div>
            <div className="col">
              <img src={NamecheapImg}></img>
            </div>
            <div className="col">
              <img src={ZoomImg}></img>
            </div>
          </div>
        </div>
        <h2>APIs Scraped:</h2>
        <p>
          <a href="https://www.drugbank.com/campaigns/drug_drug_interaction_checker">
            DrugBank
          </a>
        </p>
        <p>
          <a href="https://rapidapi.com/rnelsomain/api/drug-info-and-price-history">
            Drug Info & Price History
          </a>
        </p>
        <p>
          <a href="https://open.fda.gov/apis/drug/">
            Food & Drug Administration
          </a>
        </p>
        <h2>Our API Documentation:</h2>
        <p>
          <a href="https://documenter.getpostman.com/view/30106705/2s9YJZ3jf6">
            Drug Profiles API (Postman)
          </a>
        </p>
        <h2>GitLab Repository:</h2>
        <p>
          <a href="https://gitlab.com/idb-project-group-8/drugapp">
            drugapp Repo
          </a>
        </p>
      </div>
    </div>
  );
}

export default About;
