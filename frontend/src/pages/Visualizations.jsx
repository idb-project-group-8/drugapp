import { useState, useEffect } from "react";
import { getConditions, getDrugs, getSideEffects } from "../Database";
import SeverityPieChart from "../components/visualizations/SeverityPieChart";
import LocationBarChart from "../components/visualizations/LocationBarChart";
import MethodPieChart from "../components/visualizations/MethodPieChart";

function Visualizations() {
  const [conditions, setConditions] = useState([]);
  const [drugs, setDrugs] = useState([]);
  const [sideEffects, setSideEffects] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      const c = await getConditions();
      const d = await getDrugs();
      const se = await getSideEffects();
      setConditions(c);
      setDrugs(d);
      setSideEffects(se);
      setLoading(false);
    };
    setLoading(true);
    fetchData();
  }, []);

  if (loading) {
    return <h1>Loading...</h1>;
  }

  const data = [conditions, drugs, sideEffects];
  return (
    <div id="main_content">
      <h1>Visualizations</h1>
      <h3 className="centered-h3">Conditions and Side Effects by Severity</h3>
      <div className="centered">
        <SeverityPieChart data={data} />
      </div>
      <h3 className="centered-h3">Conditions and Side Effects by Body Part</h3>
      <div className="centered">
        <LocationBarChart data={data} />
      </div>
      <h3 className="centered-h3">Drug Method of Injection</h3>
      <div className="centered">
        <MethodPieChart data={data} />
      </div>
    </div>
  );
}

export default Visualizations;
