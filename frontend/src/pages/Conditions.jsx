import { useState, useEffect } from "react";
import { getConditions } from "../Database";
import InstanceCard from "../components/InstanceCard";
import Pagination from "../components/Pagination";
import PerPageSelector from "../components/PerPageSelector";
import Filter from "../components/Filter";
import Sort from "../components/Sort";
import "../styles/Model.css";

const filterOptions = [
  "None",
  "Severity: Mild",
  "Severity: Moderate",
  "Severity: Severe",
  "Location: Head",
  "Location: Bones/Joints",
  "Location: Skin",
  "Location: Whole Body",
];

const sortOptions = [
  "None",
  "Name (ascending)",
  "Name (descending)",
  "Severity (mild - severe)",
  "Severity (severe - mild)",
  "Location (ascending)",
  "Location (descending)",
];

const bones = [];

function Conditions() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [allConditions, setAllConditions] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filterBy, setFilterBy] = useState("None");
  const [sortBy, setSortBy] = useState("None");

  useEffect(() => {
    const fetchConditions = async () => {
      const fetchedConditions = await getConditions();
      setAllConditions(fetchedConditions);
      setLoading(false);
    };
    setLoading(true);
    fetchConditions();
  }, []);

  const handleFilter = (option) => {
    setFilterBy(option);
    setPage(1);
  };

  const handleSort = (option) => {
    setSortBy(option);
    setPage(1);
  };

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  let conditions = allConditions;
  conditions = conditions.filter((x) => {
    if (filterBy.startsWith("Severity")) {
      return x.severity === filterBy.substring(10);
    } else if (filterBy.startsWith("Location")) {
      if (filterBy.substring(10) == "Head")
        return x.location === "Head" || x.location === "Mind";
      else if (filterBy.substring(10) == "Bones/Joints")
        return x.location === "Bones" || x.location == "Joints";
      else if (filterBy.substring(10) == "Skin") return x.location === "Skin";
      else if (filterBy.substring(10) == "Whole Body")
        return x.location === "Whole body";
      else throw "Error: Unknown Filter";
    } else {
      return true;
    }
  });
  if (sortBy !== "None") {
    conditions.sort((a, b) => {
      if (sortBy === "Name (ascending)") return a.name.localeCompare(b.name);
      if (sortBy === "Name (descending)") return -a.name.localeCompare(b.name);
      if (sortBy === "Severity (mild - severe)")
        return a.severity.localeCompare(b.severity);
      if (sortBy === "Severity (severe - mild)")
        return b.severity.localeCompare(a.severity);
      if (sortBy === "Location (ascending)")
        return a.location.localeCompare(b.location);
      if (sortBy === "Location (descending)")
        return -a.location.localeCompare(b.location);
      throw "Error: Unknown Sort";
    });
  }

  const numPages = Math.ceil(conditions.length / perPage);
  return (
    <div id="instance_card_container">
      <h1>Conditions / Addictions</h1>
      <h5 className="total">
        {filterBy === "None" ? "Total number of Conditions: " : "Results: "}
        {conditions.length}
      </h5>
      <PerPageSelector setPerPage={setPerPage} setPage={setPage} />
      <Filter options={filterOptions} func={handleFilter} />
      <Sort options={sortOptions} func={handleSort} />
      <Pagination numPages={numPages} page={page} setPage={setPage} />
      <div className="container">
        <div className="row row-cols-5 g-4">
          {conditions.map((c, index) => {
            if (index >= (page - 1) * perPage && index < page * perPage) {
              return (
                <InstanceCard key={c.name} instance={c} type="Condition" />
              );
            }
          })}
        </div>
      </div>
      <Pagination numPages={numPages} page={page} setPage={setPage} />
    </div>
  );
}

export default Conditions;
