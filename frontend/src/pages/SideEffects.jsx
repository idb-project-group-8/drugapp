import { useState, useEffect } from "react";
import { getSideEffects } from "../Database";
import InstanceCard from "../components/InstanceCard";
import Pagination from "../components/Pagination";
import PerPageSelector from "../components/PerPageSelector";
import "../styles/Model.css";
import Filter from "../components/Filter";
import Sort from "../components/Sort";

const filterOptions = [
  "None",
  "Severity: Mild to moderate",
  "Severity: Mild to severe",
];

const sortOptions = [
  "None",
  "Name (ascending)",
  "Name (descending)",
  "Severity (mild - severe)",
  "Severity (severe - mild)",
  "Body Part (ascending)",
  "Body Part (descending)",
];

function SideEffects() {
  const [page, setPage] = useState(1);
  const [perPage, setPerPage] = useState(10);
  const [allSideEffects, setAllSideEffects] = useState([]);
  const [loading, setLoading] = useState(true);
  const [filterBy, setFilterBy] = useState("None");
  const [sortBy, setSortBy] = useState("None");

  useEffect(() => {
    const fetchSideEffects = async () => {
      const fetchedSideEffects = await getSideEffects();
      setAllSideEffects(fetchedSideEffects);
      setLoading(false);
    };
    setLoading(true);
    fetchSideEffects();
  }, []);

  const handleFilter = (option) => {
    setFilterBy(option);
    setPage(1);
  };

  const handleSort = (option) => {
    setSortBy(option);
    setPage(1);
  };

  if (loading) {
    return <h1>LOADING...</h1>;
  }

  let sideEffects = allSideEffects;
  sideEffects = sideEffects.filter((x) => {
    if (filterBy.startsWith("Severity")) {
      return x.severity === filterBy.substring(10);
    } else {
      return true;
    }
  });
  if (sortBy !== "None") {
    sideEffects.sort((a, b) => {
      if (sortBy === "Name (ascending)") return a.name.localeCompare(b.name);
      if (sortBy === "Name (descending)") return -a.name.localeCompare(b.name);
      if (sortBy === "Severity (mild - severe)")
        return a.severity.localeCompare(b.severity);
      if (sortBy === "Severity (severe - mild)")
        return b.severity.localeCompare(a.severity);
      if (sortBy === "Body Part (ascending)")
        return a.bodyPart.localeCompare(b.bodyPart);
      if (sortBy === "Body Part (descending)")
        return -a.bodyPart.localeCompare(b.bodyPart);
      throw "Error: Unknown Sort";
    });
  }

  const numPages = Math.ceil(sideEffects.length / perPage);
  return (
    <div id="instance_card_container">
      <h1>Side Effects</h1>
      <h5 className="total">
        {filterBy === "None" ? "Total number of Side Effects: " : "Results: "}
        {sideEffects.length}
      </h5>
      <PerPageSelector setPerPage={setPerPage} setPage={setPage} />
      <Filter options={filterOptions} func={handleFilter} />
      <Sort options={sortOptions} func={handleSort} />
      <Pagination numPages={numPages} page={page} setPage={setPage} />
      <div className="container">
        <div className="row row-cols-5 g-4">
          {sideEffects.map((s, index) => {
            if (index >= (page - 1) * perPage && index < page * perPage) {
              return (
                <InstanceCard key={s.name} instance={s} type="SideEffect" />
              );
            }
          })}
        </div>
      </div>
      <Pagination numPages={numPages} page={page} setPage={setPage} />
    </div>
  );
}

export default SideEffects;
