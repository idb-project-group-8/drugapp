import axios from "axios";

const conditions_url = "https://www.drugprofiles.me/get_conditions";
const drugs_url = "https://www.drugprofiles.me/get_drugs";
const effects_url = "https://www.drugprofiles.me/get_effects";

export const getConditions = async () => {
  const response = await axios.get(conditions_url);
  return response.data;
};
export const getDrugs = async () => {
  const response = await axios.get(drugs_url);
  return response.data;
};
export const getSideEffects = async () => {
  const response = await axios.get(effects_url);
  return response.data;
};

export const getCondition = async (cond) => {
  const response = await axios.get(conditions_url);
  const conditions = response.data;
  if (typeof cond === "string") {
    return conditions.find((c) => c.name === cond);
  } else if (typeof cond === "number") {
    return conditions[cond];
  } else {
    throw "ERROR: invalid type for cond";
  }
};

export const getDrug = async (drug) => {
  const response = await axios.get(drugs_url);
  const drugs = response.data;
  if (typeof drug === "string") {
    return drugs.find((d) => d.name === drug);
  } else if (typeof drug === "number") {
    return drugs[drug];
  } else {
    throw "ERROR: invalid type for drug";
  }
};

export const getSideEffect = async (se) => {
  const response = await axios.get(effects_url);
  const sideEffects = response.data;
  if (typeof se === "string") {
    return sideEffects.find((s) => s.name === se);
  } else if (typeof se === "number") {
    return sideEffects[se];
  } else {
    throw "ERROR: invalid type for se";
  }
};

const providersURL = "https://api.flint-aqua-advocates.me/providers";
const neighborhoodsURL = "https://api.flint-aqua-advocates.me/neighborhoods";
const restaurantsURL = "https://api.flint-aqua-advocates.me/restaurants";

export const getProviders = async () => {
  const response = await axios.get(providersURL + "?per_page=10000");
  return response.data.response;
};

export const getNeighborhoods = async () => {
  const response = await axios.get(neighborhoodsURL + "?per_page=10000");
  return response.data.response;
};

export const getRestaurants = async () => {
  const response = await axios.get(restaurantsURL + "?per_page=10000");
  return response.data.response;
};
