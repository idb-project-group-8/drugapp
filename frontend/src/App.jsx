import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import About from "./pages/About";
import Conditions from "./pages/Conditions";
import Drugs from "./pages/Drugs";
import SideEffects from "./pages/SideEffects";
import ConditionInstance from "./components/ConditionInstance";
import DrugInstance from "./components/DrugInstance";
import SideEffectInstance from "./components/SideEffectInstance";
import SearchResults from "./pages/SearchResults";
import Visualizations from "./pages/Visualizations";
import ProviderVisualizations from "./pages/ProviderVisualizations";
import "./styles/App.css";

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/About" element={<About />} />
        <Route path="/Conditions" element={<Conditions />} />
        <Route path="/Drugs" element={<Drugs />} />
        <Route path="/SideEffects" element={<SideEffects />} />
        <Route path="/Condition/:cond_id" element={<ConditionInstance />} />
        <Route path="/Drug/:drug_id" element={<DrugInstance />} />
        <Route path="/SideEffect/:se_id" element={<SideEffectInstance />} />
        <Route path="/Visualizations" element={<Visualizations />} />
        <Route
          path="/Provider_Visualizations"
          element={<ProviderVisualizations />}
        />
        <Route path="/search/:sid" element={<SearchResults />} />
      </Routes>
      <Footer />
    </Router>
  );
}

export default App;
