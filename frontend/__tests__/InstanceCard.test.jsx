import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import InstanceCard from "../src/components/InstanceCard";

test("renders learn react link", () => {
  const drugInstance = {
    name: "drugName",
    route: "drugRoute",
    manufacturer: "drugManufacturer",
    conditions: "",
    sideEffects: "",
    img1: null,
    img2: null,
  };
  render(<InstanceCard instance={drugInstance} type="Drug" />);
  expect(screen.getByText("drugName")).toBeInTheDocument();
  expect(screen.getByText("Route: drugRoute")).toBeInTheDocument();
  expect(
    screen.getByText("Manufacturer: drugManufacturer")
  ).toBeInTheDocument();
});
