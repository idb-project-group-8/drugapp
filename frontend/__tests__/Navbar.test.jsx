import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Navbar from "../src/components/Navbar";

test("renders learn react link", () => {
  render(<Navbar />);
  expect(screen.getByText("Home")).toBeInTheDocument();
  expect(screen.getByText("About")).toBeInTheDocument();
  expect(screen.getByText("Conditions/Addictions")).toBeInTheDocument();
  expect(screen.getByText("Drugs/Medications")).toBeInTheDocument();
  expect(screen.getByText("Side-Effects")).toBeInTheDocument();
});
