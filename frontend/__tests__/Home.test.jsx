import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Home from "../src/pages/Home";

test("renders learn react link", () => {
  render(<Home />);
  expect(screen.getByText("Drug Profiles")).toBeInTheDocument();
});
