from selenium import webdriver
import time
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver import ChromeOptions
from webdriver_manager.chrome import ChromeDriverManager


class FAATest(unittest.TestCase):
    def setUp(self) -> None:
        options = ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()), options=options
        )
        self.driver.get("https://www.drugprofiles.me/")
        self.driver.maximize_window()
        self.driver.implicitly_wait(5)

    def test_title(self) -> None:
        title = self.driver.title
        self.assertEqual(title, "Drug Profiles")

    def test_open_about(self) -> None:
        h1 = self.driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Drug Profiles")

    def test_home_button(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Home").click()
        h1 = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Drug Profiles")

    def test_sideeffects_page(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Side-Effects").click()
        h1 = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Side-Effects")

    def test_conditions_page(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Conditions/Addictions").click()
        h1 = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Conditions / Addictions")

    def test_drugs_page(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Drugs/Medications").click()
        h1 = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Drugs / Medications")

    def test_instance_page(self) -> None:
        driver = self.driver
        driver.find_element(By.LINK_TEXT, "Drugs/Medications").click()
        driver.find_element(By.LINK_TEXT, "Opioid Addiction").click()
        h1 = driver.find_element(By.TAG_NAME, "h1")
        self.assertEqual(h1.text, "Opioid Addiction")

    def tearDown(self) -> None:
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
