**Canvas / Ed Discussion group number (please check this carefully)**

52985_08 (Group 8)

**Names of the team members**

Sam Lu

Matthew Larson

Rayan Ali

Brandon Boettcher

**git SHA**
SHA: 4e12f91c6dad3bbac28cfe601ec5c8cdbbbbcaff

**Project Work Time (combined)**
Estimated: 15 hours
Actual: 20 hours

**Phase Leaders**
We don't really have phase leaders, we're just working together to 
produce everything needed.

**Name of the project (alphanumeric, no spaces, max 32 chars; this will also be your URL)**

https://www.DrugProfiles.me

**The proposed project:**

A website providing information on conditions, treatments, and side-effects specifically geared-towards the Appalachian communities which struggle with drug addiction that has been neglected since the 20th century. These communities are predominantly White with blue-collared jobs, are an aging population, and have struggled economically due to decline of traditional industries like coal and manufacturing. This information could potentially help them understand their conditions and treatment possibilities and side effects better to avoid misuse of drugs/medicine.

**URLs of at least three data sources that you will programmatically scrape using a RESTful API (be very sure about this)**

1. https://www.drugbank.com/campaigns/drug_drug_interaction_checker
2. https://rapidapi.com/rnelsomain/api/drug-info-and-price-history
3. https://open.fda.gov/apis/drug/

**At least three models**

1. Conditions/Addictions common in Appalachian communities
2. Drugs/Medications frequently abused in Appalachian communities
3. Side-effects of above drugs and conditions

**An estimate of the number of instances of each model**

1. Conditions/Addictions common in Appalachian communities (50+)
2. Drugs/Medications frequently abused in Appalachian communities (150+)
3. Side-effects (~100)

**Each model must have many attributes. Describe five of those attributes for each model that you can filter or sort**

1. Conditions/Addictions common in Appalachian communities
    - Part of Body the Condition Affects
    - Condition Name
    - Location where condition is prevalent
    - Drugs used to treat it
    - Side-effects

2. Drugs/Medications frequently abused in Appalachian communities
    - Drug Name
    - Method of Ingestion
    - Side-effects
    - Chemicals/Ingredients
    - FDA approved

3. Side-effects
    - Potential condition cause
    - Potential drugs cause
    - Part of Body it affects
    - Name
    - Time it lasts

**Instances of each model must connect to instances of at least two other models**

**Instances of each model must be rich with different media (e.g., feeds, images, maps, text, videos, etc.) (be very sure about this)**

**Describe two types of media for instances of each model**

1. Conditions/Addictions common in Appalachian communities
    - Pictures
    - GIF of cartoon person with affected area highlighted

2. Drugs/Medications frequently abused in Appalachian communities
    - Pictures
    - GIF of method of ingestion

3. Side-effects in Appalachian communities
    - Pictures
    - GIF of cartoon person with affected area highlighted


**Describe three questions that your site will answer**

1. What drugs are abused commonly in my area(Appalachian region)?
2. What conditions are so common in my area(Appalachian region) that are making people feel the need to abuse these drugs?
3. What side effects could this drug cause that I should be aware of?

*(Disclaimer: Not medical advice. For actual medical advice, consult a doctor.)
